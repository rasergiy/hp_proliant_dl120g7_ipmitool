# HP ProLiant DL120 G7 template via ipmitool 

This is HP ProLiant DL170G7 template and script to gather information 
via ipmitool and send it to zabbix via zabbix trapper.
ipmitool shows analog sensors, and freeipmi used by zabbix is using 
discreet sensor values. So the only way to get some analog sensors 
values from some ProLiant servers, without zabbix modification is to
use ipmitool

More information about analog and discreet sensors in ProLiant can be 
found at:

1. http://hotokblog.blogspot.ru/2012/11/remote-ipmi-hp-proliant-dl380p-g8.html
2. https://support.zabbix.com/browse/ZBX-8395


# Usage

1. Install given xml template to the zabbix
2. Write configuration file as shown in the example
2. Copy script to the server, and run it by cron

#Copyright

Copyright (c) 2016 Sergey Raskin. E-Mail: rasergiy@gmail.com
This program are distributed under the MIT licence.
